# pip3 install boto3
#

# https://stackoverflow.com/questions/57012709/aws-boto3-how-to-list-of-all-the-org-idseven-nested-under-organization
# https://stackoverflow.com/questions/56744368/list-resource-for-child-account-from-master-account-using-boto3
# https://stackoverflow.com/questions/61914154/boto3-list-all-accounts-in-an-organization

# aws --profile=organizations_readonly organizations list-accounts

import boto3
AWS_Profile_Name = 'my-aws-profile-name'
AWS_Region = 'ap-southeast-2'

def getAccountnumberslist(search_string):
    boto3.setup_default_session(profile_name=AWS_Profile_Name,region_name=AWS_Region)
    org = boto3.client('organizations')
    paginator = org.get_paginator('list_accounts')
    page_iterator = paginator.paginate()

    account_id=[]   
    temp_array=[]
    return_array=[]
    account_tags={}

    # The code below pulls all the AWS account numbers out of Organizations and first places them into an array or list by
    # appending each number
    #
    for page in page_iterator:        
        for acct in page['Accounts']:
            account_id.append(acct)

    # Loop through the array of returned AWS account numbers, and then query each account for its tags.
    # Append Account_Id to the end of the returned tag and add the account number as the value
    # Push the modified returned tag into the temp_array array
    for account_number in account_id:
        returned_tags = org.list_tags_for_resource(
        ResourceId=account_number['Id']
        )
        returned_tags['Account_Id'] = account_number['Id']
        temp_array.append(returned_tags)

    account_tags=temp_array

    for dict in account_tags:
        temp_string = str(dict)
        if search_string in temp_string:
            return_array.append(dict['Account_Id'])
    return (return_array)

print(getAccountnumberslist("\'test-env\'"))