# getAccountnumberslist

This is a Python3 script that connects to AWS Organizations, and tries to match a search string to tags. It then returns an array containing the account numbers whose tags match the search string. The script makes use of the boto3 library.

## Getting Started
You will need a working copy of Python3 and have the boto3 library installed on your local machine.

### Prerequisites
You will need to have an IAM user created in the main AWS account, with read-only access to Organizations. You will need to have an AWS access key and secret key located in ```~/.aws/credentials```. For example:
```
[organizations_readonly]
aws_access_key_id = XXXXXXXXXXXXXXXXXXXX
aws_secret_access_key = XXXXXXXXXXXXXXXX
```
## Running the script
This is an example of the script being called:
```
getAccountnumberslist("\'test-environment\'")
['012345678901', '012345678901', '012345678901', '012345678901', '012345678901', '012345678901', '012345678901']
```